import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
} from 'react-native';

import styles from '../Styles'

class SearchBar extends React.Component {

    render() {
        return (
            <View style={styles.listview.searchBar}>
                <TextInput
                autoCapitalize='none'
                autoCorrect={false}
                placeholder='Search for medi in iTuens...'
                enablesReturnKeyAutomatically={true}
                style={styles.listview.searchBarInput}
                onEndEditing={this.props.onSearch}
                >
                </TextInput>
            </View>
        )
    }
}

export default SearchBar
