import React from 'react';
import {
    StyleSheet,
} from 'react-native';

export default global =  StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    content: {
        paddingTop : 64,
        flex: 1,
        backgroundColor: '#EFEFEF'
    },
});