import React from 'react';
import {
    StyleSheet,
} from 'react-native';

export default listview =  StyleSheet.create({
    searchBar: {
        marginTop: 0,
        padding: 3,
        paddingLeft: 8,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    searchBarInput: {
        height: 30,
        fontSize: 15,
        flex: 1,
    },
});