import React from 'react';
import {
    StyleSheet,
} from 'react-native';

import global from './styles/global';
import listview from './styles/listview';



export default styles = {
    global : global,
    listview : listview
};