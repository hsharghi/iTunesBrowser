import React from 'react';
import {
  StyleSheet,
  Text,
  StatusBar,
  View,
  NavigatorIOS,
  AlertIOS,
  PropTypes,
} from 'react-native';

import styles from './Styles'
import MediaListView from './components/media-list-view'

console.log(styles.navbar);
StatusBar.setBarStyle('light-content');
// console.log('now here come the styles...');
// console.log(styles2.content2);

export default class App extends React.Component {
  render() {
    return (
      <NavigatorIOS
        initialRoute={{
          component: MediaListView,
          title: 'iTunesBrowser',
          rightButtonTitle: 'Search',
          onRightButtonPress: () => AlertIOS.alert(
            'title', 'search pressed'
          )
        }}
        style={styles.global.mainContainer}
        barTintColor='#2A3744'
        titleTextColor='#FEFEFE'
        tintColor='#FEFEFE'
      />
    );
  }
}

// var styles = StyleSheet.create({
//   mainContainer: {
//     flex: 1
//   },
//   content: {
//     flex: 1,
//     backgroundColor: '#FF3366'
//   },
//   navbar: {
//     backgroundColor: '#2A3744',
//     paddingTop: 30,
//     paddingBottom: 10,
//     flexDirection: 'row'
//   },
//   navbarTitle: {
//     color: '#FEFEFE',
//     textAlign: 'center',
//     fontWeight: 'bold',
//     flex: 1,
//   },
//   navbarButton: {
//     width: 50,
//     color: '#FEFEFE',
//     textAlign: 'center',
//   }
// });